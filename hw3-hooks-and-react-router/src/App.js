import {useState, useEffect} from 'react';
import './App.scss';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';
import ProductsList from "./components/ProductsList";
import Header from "./components/Header";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import FavouritesPage from "./pages/FavouritesPage";
import CartPage from "./pages/CartPage";
import HomePage from "./pages/HomePage";
import ErrorPage from "./pages/ErrorPage/ErrorPage";
import SharedLayout from "./components/SharedLayout";

export const favouritesArr = JSON.parse(localStorage.getItem('favouritesIDs')) ?? [];
export const productsCartArr = JSON.parse(localStorage.getItem('productsCartArr')) ?? [];

function App() {
    const [isModalOpen, setOpenModal] = useState(false);
    const [modalData, setModalData] = useState({
        header: '',
        closeButton: false,
        text: '',
        actions: null,
    });
    const {header, closeButton, text, actions} = modalData;

    const [products, setProducts] = useState([]);

    const [cart, setCart] = useState(productsCartArr);
    const [favourites, setFavourites] = useState(favouritesArr);

    useEffect(() => {
        const fetchProducts = async () => {
            const {game_devices: gameDevices} = await fetch(`./game-devices.json`).then(res => res.json());
            setProducts(gameDevices);
            if (products.length !== 0) {
                products.forEach(item => {
                    const key = item.productArticle;
                    const value = JSON.stringify(item);
                    localStorage.setItem(key, value);
                })
            }
        }

        fetchProducts()
            .catch(console.error);
    }, [products.length]);

    const openModal = ({header, closeButton, text, actions}) => {
        setOpenModal(true);
        setModalData({
            header,
            closeButton,
            text,
            actions,
        })
    }

    const closeModal = () => setOpenModal(false);

    const addToCart = (product) => {
        setCart([...cart, product]);
        localStorage.setItem('productsCartArr', JSON.stringify([...cart, product]));
        closeModal();
    }

    const deleteFromCart = (product) => {
        const currentCart = [...cart].filter(item => item.productArticle !== product.productArticle);
        setCart([...currentCart]);
        localStorage.setItem('productsCartArr', JSON.stringify([currentCart]));
        closeModal();
    }

    const changeFavourites = (favArr) => setFavourites([...favArr]);

    return (<>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<SharedLayout cart={cart} favourites={favourites}/>}>
                        <Route index element={<HomePage products={products}
                                                        addToCart={addToCart}
                                                        changeFavourites={changeFavourites}
                                                        openModal={openModal}
                                                        closeModal={closeModal}
                                                        favourites={favourites}/>}/>
                        <Route path='cart' element={<CartPage cart={cart}
                                                              addToCart={addToCart}
                                                              changeFavourites={changeFavourites}
                                                              openModal={openModal}
                                                              closeModal={closeModal}
                                                              favourites={favourites}
                                                              deleteFromCart={deleteFromCart}/>}/>
                        <Route path='favourites' element={<FavouritesPage products={products}
                                                                          addToCart={addToCart}
                                                                          changeFavourites={changeFavourites}
                                                                          openModal={openModal}
                                                                          closeModal={closeModal}
                                                                          favourites={favourites}/>}/>
                        <Route path='*' element={<ErrorPage/>}/>
                    </Route>
                </Routes>
            </BrowserRouter>
            {
                isModalOpen &&
                <Modal header={header} closeButton={closeButton} text={text} actions={actions} closeModal={closeModal}/>
            }
        </>
    );
}

export default App;
