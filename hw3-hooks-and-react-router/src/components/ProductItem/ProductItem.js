import React from 'react';
import PropTypes from 'prop-types';
import styles from './ProductItem.module.scss';
import Button from "../Button";
import {useState} from "react";
import FavouriteStarSVG from "../FavouriteStarSVG";
import DeleteBinSVG from "../DeleteBinSVG";

const ProductItem = (props) => {
    const {product, addToCart, changeFavourites, openModal, closeModal, favourites, inCart, deleteFromCart} = props;
    const {name, price, url: imgUrl, color} = product;

    const setUpColor = () => {
        if (localStorage.getItem('favouritesIDs')) {
            const setUpColorArr = JSON.parse(localStorage.getItem('favouritesIDs'));
            if (setUpColorArr.includes(product.productArticle)) {
                return '#ffaa00';
            }
        }
        return 'white';
    }

    const [starFavSVG, setStarFavSVG] = useState(() => setUpColor());
    const colorToggleHandler = () => setStarFavSVG(prevColor => (prevColor === '#ffaa00' ? 'white' : '#ffaa00'));

    const toggleFavourites = () => {
        const favouritesCurrentArr= [...favourites]
        colorToggleHandler();
        if (favouritesCurrentArr.includes(product.productArticle)) {
            favouritesCurrentArr.splice(favouritesCurrentArr.indexOf(product.productArticle), 1);

        } else {
            favouritesCurrentArr.push(product.productArticle)
        }
        localStorage.setItem('favouritesIDs', JSON.stringify(favouritesCurrentArr));

        changeFavourites(JSON.parse(localStorage.getItem('favouritesIDs')));
    }

    return (
        <div className={styles.productItem}>
            <img src={imgUrl} alt={name} className={styles.productImage}/>
            <div className={styles.productInfo}>
                <h3 className={styles.productName}>{name}</h3>
                <p className={styles.productPrice}>{price}</p>
                <p className={styles.productColor}>Color: {color}</p>
                <Button onClick={() => openModal({
                    header: 'Do you want to add this product to cart?',
                    closeButton: true,
                    text: (
                        <>
                            You can still delete the product from cart later.
                            <br/>
                            Are you sure you want to add it to Cart?
                        </>
                    ),
                    actions: (<>
                        <Button bgColor={'#b43725'} text={'Yes'} onClick={() => addToCart(product)}/>
                        <Button bgColor={'#b43725'} text={'Cancel'} onClick={() => closeModal()}/>
                    </>)

                })} bgColor={'red'} text={'Add to cart'}/>
                {!inCart && <FavouriteStarSVG className={styles.productFavourite} color={starFavSVG} onClick={toggleFavourites}
                                  stroke={'black'}/>
                }
                {
                    inCart && <DeleteBinSVG className={styles.delFromCart} color={'#000000'} onClick={() => openModal({
                        header: 'Do you want to add this product to cart?',
                        closeButton: true,
                        text: (
                            <>
                                You can always add the product on home page later.
                                <br/>
                                Are you sure you want to delete the product from Cart?
                            </>
                        ),
                        actions: (<>
                            <Button bgColor={'#b43725'} text={'Yes'} onClick={() => deleteFromCart(product)}/>
                            <Button bgColor={'#b43725'} text={'Cancel'} onClick={() => closeModal()}/>
                        </>)

                    })}/>
                }
            </div>
        </div>
    );
};

ProductItem.propTypes = {
    // name: PropTypes.string.isRequired,
    // price: PropTypes.number.isRequired,
    // url: PropTypes.string.isRequired,
    // color: PropTypes.string.isRequired,
    // isFavourite: PropTypes.bool.isRequired,
    addToCart: PropTypes.func,
};

ProductItem.defaultProps = {
    inCart: false,
}
export default ProductItem;