import React from 'react';
import {Link} from "react-router-dom";
import styles from './ErrorPage.module.scss';

const ErrorPage = () => {
    return (
        <div>
            <h3 className={styles.title}>404</h3>
            <p className={styles.message}>Page not found</p>
            <Link className={styles.backToHomeBTN} to='/'>Back home</Link>
        </div>
    );
};

export default ErrorPage;