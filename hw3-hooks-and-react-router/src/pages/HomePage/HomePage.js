import React from 'react';
import PropTypes from 'prop-types';
import styles from './HomePage.module.scss';
import ProductsList from "../../components/ProductsList";

const HomePage = ({products, addToCart, changeFavourites, openModal, closeModal, favourites}) => {

    return (<>
            <h3 className={styles.title}>All products</h3>
            {
                products.length !== 0 &&
                <ProductsList products={products} addToCart={addToCart} changeFavourites={changeFavourites}
                              openModal={openModal} closeModal={closeModal} favourites={favourites}/>
            }
        </>
    );
};

HomePage.propTypes = {};

export default HomePage;