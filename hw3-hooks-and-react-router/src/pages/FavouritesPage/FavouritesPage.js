import React from 'react';
import PropTypes from 'prop-types';
import styles from './Favourites.module.scss';
import ProductItem from "../../components/ProductItem";

const FavouritesPage = props => {
    const {products, favourites, addToCart, changeFavourites, openModal, closeModal} = props;
    return (
        <>
            <h3 className={styles.favouritesTitle}>Favourite Items</h3>
            <div className={styles.favouriteItemsWrapper}>
                {
                    favourites.length === 0 && <p className={styles.noFavProducts}>No favourite products</p>
                }
                {
                    favourites.length !== 0 && products.map((item) => {
                        if (favourites.includes(item.productArticle)) {
                            return <ProductItem key={item.productArticle}
                                                product={item}
                                                addToCart={addToCart}
                                                changeFavourites={changeFavourites}
                                                openModal={openModal}
                                                closeModal={closeModal}
                                                favourites={favourites}
                            />
                        }
                    })
                }
            </div>
        </>
    );
};

FavouritesPage.propTypes = {};

export default FavouritesPage;