import React from 'react';
import PropTypes from 'prop-types';
import styles from './CartPage.module.scss';
import ProductItem from "../../components/ProductItem";

const CartPage = (props) => {
    const {cart, favourites, addToCart, changeFavourites, openModal, closeModal, deleteFromCart} = props;
    const inCart = true;
    console.log(cart);
        return (
            <>
                <h3 className={styles.title}>Cart items</h3>
                <div className={styles.cartItemWrapper}>
                    {
                        cart.length === 0 && <p className={styles.noCartItems}>Cart is empty</p>
                    }
                    {
                        cart.length !== 0 && cart.map(item => {
                            return <ProductItem key={item.productArticle}
                                                product={item}
                                                addToCart={addToCart}
                                                changeFavourites={changeFavourites}
                                                openModal={openModal}
                                                closeModal={closeModal}
                                                favourites={favourites}
                                                deleteFromCart={deleteFromCart}
                                                inCart={inCart}
                            />
                        })
                    }
                </div>
            </>
        );
};

CartPage.propTypes = {};

export default CartPage;