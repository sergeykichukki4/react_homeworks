import {setupStore} from "../store/index";

export const prepareInitState = () => {
    const cartList = JSON.parse(localStorage.getItem('productsCartArr')) ?? [];
    const totalQuantity = JSON.parse(localStorage.getItem('productsCartArr'))?.length ?? 0;
    const favouritesList = JSON.parse(localStorage.getItem('favouritesIDs')) ?? [];
    const favouritesTotal = JSON.parse(localStorage.getItem('favouritesIDs'))?.length ?? 0;

    return setupStore({
        cart: {
            cartList, totalQuantity
        },
        favourites: {
            favouritesList, favouritesTotal
        },
        products: {
            productsList: []
        },
        modal: {
            isOpen: false,
            modalData: {
                header: '',
                text: '',
                actions: null,
            }
        }
    });
}