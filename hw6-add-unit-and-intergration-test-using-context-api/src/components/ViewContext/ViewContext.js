import React, {useContext, useState} from 'react';

const ViewContext = React.createContext();
const ViewUpdateContext = React.createContext();

export const useView = () => useContext(ViewContext);
export const useViewUpdate = () => useContext(ViewUpdateContext);

const ViewProvider = ({children}) => {
    const [viewSwitcher, setViewSwitcher] = useState(true);

    const toggleViewSwitcher = () => setViewSwitcher(prevViewSwitcher => !prevViewSwitcher);
    return (
        <ViewContext.Provider value={viewSwitcher}>
            <ViewUpdateContext.Provider value={toggleViewSwitcher}>
                {children}
            </ViewUpdateContext.Provider>
        </ViewContext.Provider>
    );
};

export default ViewProvider;