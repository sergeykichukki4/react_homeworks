import {screen, render} from "@testing-library/react";
import DeleteBinSVG from "./DeleteBinSVG";

describe('DeleteBinSVG tests: ', () => {
    test('Should work', () => {
        const handleClick = jest.fn();
        render(<DeleteBinSVG color={'#000000'} data='delete-from-cart-svg' onClick={handleClick}/>);
    })

    test('Should work', () => {
        const handleClick = jest.fn();
        const deleteBtn = render(<DeleteBinSVG color={'#000000'} data='delete-from-cart-svg' onClick={handleClick}/>);

        expect(deleteBtn).toMatchSnapshot();
    })
})