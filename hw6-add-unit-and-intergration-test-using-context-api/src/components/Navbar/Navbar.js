import React from 'react';
import {Link} from "react-router-dom";
import styles from './Navbar.module.scss';
const Navbar = () => {
    return (
        <nav className={styles.navbar}>
            <Link className={styles.navItem} to='/'>Home</Link>
            <Link className={styles.navItem} to='cart'>Cart</Link>
            <Link className={styles.navItem} to='favourites'>Favourites</Link>
        </nav>
    );
};

export default Navbar;