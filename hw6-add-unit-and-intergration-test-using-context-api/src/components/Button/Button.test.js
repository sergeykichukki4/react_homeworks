import {screen, render, fireEvent} from "@testing-library/react";
import '@testing-library/jest-dom'
import Button from "./Button";

describe('Button tests: ', () => {
    test('should be clickable and have text', () => {
        const handleClick = jest.fn();
        const btnText = 'Button test';

        render(<Button onClick={handleClick} text={btnText}/>);

        const btn = screen.getByText('Button test');

        fireEvent.click(btn);

        expect(handleClick).toHaveBeenCalledTimes(1);
    })

    test('Snapshot', () => {
        const handleClick = jest.fn();
        const btnText = 'Button test';

        const btn = render(<Button onClick={handleClick} text={btnText}/>);

        expect(btn).toMatchSnapshot();
    })
});