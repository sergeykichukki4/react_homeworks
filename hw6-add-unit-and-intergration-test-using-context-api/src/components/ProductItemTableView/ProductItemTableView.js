import React, {useState} from 'react';
import styles from './ProductItemTableView.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {favouritesActions} from "../../store/favourites-slice";
import {modalActions} from "../../store/modal-slice";
import FavouriteStarSVG from "../FavouriteStarSVG";
import Button from "../Button";


const ProductItemTableView = (props) => {
    const favourites = useSelector(state => state.favourites.favouritesList);
    const dispatch = useDispatch();
    const {name, price, url, color, productArticle} = props;
    const setUpColor = () => {
        if (favourites.includes(productArticle)) {
            return '#ffaa00';
        }
        return 'white';
    }

    const [starFavSVG, setStarFavSVG] = useState(() => setUpColor());

    const colorToggleHandler = () => setStarFavSVG(prevColor => (prevColor === '#ffaa00' ? 'white' : '#ffaa00'));

    const toggleFavourites = () => {
        const favouritesCurrentArr = [...favourites];
        colorToggleHandler();
        if (favouritesCurrentArr.includes(productArticle)) {
            favouritesCurrentArr.splice(favouritesCurrentArr.indexOf(productArticle), 1);
            dispatch(favouritesActions.removeFromFavourites(productArticle));

        } else {
            favouritesCurrentArr.push(productArticle);
            dispatch(favouritesActions.addToFavourites(productArticle));
        }
        localStorage.setItem('favouritesIDs', JSON.stringify(favouritesCurrentArr));
    }

    const openModal = ({header, text, actions}) => {
        dispatch(modalActions.openModal({
            header,
            text,
            actions,
        }))
    }
    return (
        <tr className={styles.productItem}>
            <td>
                <img src={url} alt={name} className={styles.productImage}/>
            </td>
            <td className={styles.productInfo}>
                <h3 className={styles.productName}>{name}</h3>
            </td>
            <td className={styles.productInfo}>
                <p className={styles.productPrice}>{price}</p>
            </td>
            <td className={styles.productInfo}>
                <p className={styles.productColor}>Color: {color}</p>
            </td>
            <td>
                <Button onClick={() => openModal({
                    header: 'Do you want to add this product to cart?',
                    text: `You can still delete the product from cart later.\nAre you sure you want to add it to Cart?`,
                    actions: `addToCart,${name},${price},${url},${productArticle},${color}`

                })} bgColor={'red'} text={'Add to cart'}/>
            </td>
            <td>
                <FavouriteStarSVG className={styles.productFavourite}
                                  data={'toggle-favourites-svg'}
                                  color={starFavSVG}
                                  onClick={toggleFavourites}
                                  stroke={'black'}
                                  width={'50'}
                                  height={'50'}
                />
            </td>
        </tr>
    );
};

export default ProductItemTableView;