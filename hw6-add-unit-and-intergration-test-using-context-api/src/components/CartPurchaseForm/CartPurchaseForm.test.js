import {renderWithProviders} from "../../utils/test-utils";
import CartPurchaseForm from "./CartPurchaseForm";
import {screen} from "@testing-library/react";

describe(`Purchase form test`, () => {
    test('Should work', () => {
        const purchaseForm = renderWithProviders(<CartPurchaseForm/>);

    })

    test('Snapshot', () => {
        const purchaseForm = renderWithProviders(<CartPurchaseForm/>);
        expect(purchaseForm).toMatchSnapshot();
    })

    test('Check purchase button in DOM', () => {
        renderWithProviders(<CartPurchaseForm/>);

        screen.getByText('Checkout');
    })
})
