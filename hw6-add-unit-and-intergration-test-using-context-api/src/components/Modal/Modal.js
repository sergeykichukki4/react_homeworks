import React from 'react';
import styles from "./Modal.module.scss";
import Button from "../Button";
import {useDispatch, useSelector} from "react-redux";
import {modalActions} from "../../store/modal-slice";
import {cartActions} from "../../store/cart-slice";


const Modal = () => {
    const dispatch = useDispatch();
    const modalData = useSelector((state) => state.modal.modalData);
    const {header, text, actions} = modalData;

    const actionsData = actions.split(',');

    let productArticleForDeleteFromCart = actionsData?.length === 2 ? actionsData[1] : null;

    let productDetailsForAddToCart = actionsData?.length === 6 ? {
        name: actionsData[1],
        price:actionsData[2],
        url: actionsData[3],
        productArticle: actionsData[4],
        color: actionsData[5],
    } : null;

    const modalAction = actionsData[0];

    const closeModal = () => {
        dispatch(modalActions.closeModal());
    }

    const addToCart = (actionData) => {
        dispatch(cartActions.addToCart(actionData))
        closeModal();
    }

    const deleteFromCart = (actionData) => {
        dispatch(cartActions.deleteFromCart(actionData));
        closeModal();
    }

    return (
        <>
            <div className={styles.backgroundColor} onClick={(e) => {
                if (e.target.classList.contains(styles.backgroundColor)) {
                    closeModal();
                }
            }}>
                <div className={styles.modalContainer}>
                    <div className={styles.header}>
                        <div className={styles.title}>
                            <h2>{header}</h2>
                        </div>
                        <button onClick={closeModal} className={styles.cross}><img src="../cross_icon_white.svg"
                                                                                   width="30" height="30"
                                                                                   alt="cross-close"/></button>
                    </div>
                    <div className={styles.body}>
                        <p className={styles.text}>{text}</p>
                    </div>
                    <div className={styles.footer}>
                        {modalAction === 'addToCart' && <>
                            <Button bgColor={'#b43725'} text={'Yes'} onClick={() => addToCart(productDetailsForAddToCart)}/>
                            <Button bgColor={'#b43725'} text={'Cancel'} onClick={() => closeModal()}/>
                        </>}
                        {modalAction === 'deleteFromCart' && <>
                            <Button bgColor={'#b43725'} text={'Yes'} onClick={() => deleteFromCart(productArticleForDeleteFromCart)}/>
                            <Button bgColor={'#b43725'} text={'Cancel'} onClick={() => closeModal()}/>
                        </>}
                    </div>
                </div>
            </div>
        </>
    )
}

export default Modal;
