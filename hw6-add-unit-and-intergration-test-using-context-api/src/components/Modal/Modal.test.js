import {renderWithProviders} from "../../utils/test-utils";
import Modal from "./Modal";
import {screen, fireEvent} from "@testing-library/react";

describe('Modal tests: ', () => {
    test('Should work', () => {
        const preloadedState = {
            modal: {
                isOpen: false,
                modalData: {
                    header: '',
                    text: '',
                    actions: 'deleteFromCart,GCX123',
                }
            },
        }
        renderWithProviders(<Modal/>, {preloadedState});
    })

    test('AddToCart button should add item to CART', () => {
        const preloadedState = {
            modal: {
                isOpen: true,
                modalData: {
                    header: '',
                    text: '',
                    actions: 'addToCart,Gaming Console X,299.99,./gaming_console_x.jpg,GCX123,Black',
                }
            },
        }
        const result = renderWithProviders(<Modal/>, {preloadedState});

        const addToCartBtn = screen.getByText('Yes');
        fireEvent.click(addToCartBtn);

        const cart = result.store.getState().cart;
        const expected = {
            cartList: [{
                "name": "Gaming Console X",
                "price": 299.99,
                "url": "./gaming_console_x.jpg",
                "productArticle": "GCX123",
                "color": "Black",
                "quantity": 1,
                "totalPrice": 299.99
            }],
            totalQuantity: 1,
        }
        expect(cart).toEqual(expected);
    })

    test('Delete button should delete item from CART', () => {
        const preloadedState = {
            modal: {
                isOpen: true,
                modalData: {
                    header: '',
                    text: '',
                    actions: 'deleteFromCart,GCX123',
                }
            },
            cart: {
                cartList: [{
                    "name": "Gaming Console X",
                    "price": 299.99,
                    "url": "./gaming_console_x.jpg",
                    "productArticle": "GCX123",
                    "color": "Black",
                    "quantity": 1,
                    "totalPrice": 299.99
                }],
                totalQuantity: 1,
            },
        }
        const result = renderWithProviders(<Modal/>, {preloadedState});

        const deleteToCartBtn = screen.getByText('Yes');
        fireEvent.click(deleteToCartBtn);

        const cart = result.store.getState().cart;
        const expected = {
            cartList: [],
            totalQuantity: 0,
        }
        expect(cart).toEqual(expected);
    })

    test('Check cancel button when tyring to delete items', () => {
        const preloadedState = {
            modal: {
                isOpen: true,
                modalData: {
                    header: '',
                    text: '',
                    actions: 'deleteFromCart,GCX123',
                }
            },
            cart: {
                cartList: [{
                    "name": "Gaming Console X",
                    "price": 299.99,
                    "url": "./gaming_console_x.jpg",
                    "productArticle": "GCX123",
                    "color": "Black",
                    "quantity": 1,
                    "totalPrice": 299.99
                }],
                totalQuantity: 1,
            },
        }
        const result = renderWithProviders(<Modal/>, {preloadedState});

        const CancelBtn = screen.getByText('Cancel');
        fireEvent.click(CancelBtn);

        const cart = result.store.getState().cart;
        const expected = {
            cartList: [{
                "name": "Gaming Console X",
                "price": 299.99,
                "url": "./gaming_console_x.jpg",
                "productArticle": "GCX123",
                "color": "Black",
                "quantity": 1,
                "totalPrice": 299.99
            }],
            totalQuantity: 1,
        }
        expect(cart).toEqual(expected);
    })
});