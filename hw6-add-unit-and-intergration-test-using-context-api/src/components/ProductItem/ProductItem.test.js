import {renderWithProviders} from "../../utils/test-utils";
import {fireEvent, getByRole, getByTestId, screen} from "@testing-library/react";
import '@testing-library/jest-dom'
import ProductItem from "./ProductItem";

describe(`Product items tests: `, () => {
    test('should work', () => {
        renderWithProviders(<ProductItem
            key="key"
            name="name"
            price="12.98"
            url="http://localhost:3000/images/1.jpg"
            productArticle="productArticle"
            color="#ffaa00"
            quantity={1}
            totalPrice="64.9"
            inCart={true}
        />);
    });

    test('should open modal', () => {
        //init
        const result = renderWithProviders(<ProductItem
            key="key"
            name="name"
            price="12.98"
            url="http://localhost:3000/images/1.jpg"
            productArticle="productArticle"
            color="#ffaa00"
            quantity={1}
            totalPrice="64.9"
            inCart={true}
        />);

        //invoke
        fireEvent.click(screen.getByRole('button', { name: /Add to cart/i }));

        const modal = result.store.getState().modal;

        //check
        expect(modal).toEqual({
            isOpen: true,
            modalData: {
                actions: 'addToCart,name,12.98,http://localhost:3000/images/1.jpg,productArticle,#ffaa00',
                header: 'Do you want to add this product to cart?',
                text: 'You can still delete the product from cart later.\nAre you sure you want to add it to Cart?',
            }
        });
    });

    test('should toggle product to favourites', () => {
        //init
        const result = renderWithProviders(<ProductItem
            key="key"
            name="name"
            price="12.98"
            url="http://localhost:3000/images/1.jpg"
            productArticle="productArticle"
            color="#ffaa00"
            quantity={1}
            totalPrice="64.9"
            inCart={false}
        />);

        //invoke
        fireEvent.click(screen.getByTestId('toggle-favourites-svg'));

        const addToFavourites = result.store.getState().favourites;

        //check
        expect(addToFavourites).toEqual({
            favouritesList: ['productArticle'],
            favouritesTotal: 1,
        })

        fireEvent.click(screen.getByTestId('toggle-favourites-svg'));

        const removeFromFavourites = result.store.getState().favourites;

        expect(removeFromFavourites).toEqual({
            favouritesList: [],
            favouritesTotal: 0,
        })
    });

    test('should have delete product button when in Cart', () => {
        //init
        const result = renderWithProviders(<ProductItem
            key="key"
            name="name"
            price="12.98"
            url="http://localhost:3000/images/1.jpg"
            productArticle="productArticle"
            color="#ffaa00"
            quantity={1}
            totalPrice="64.9"
            inCart={true}
        />);

        screen.getByTestId('delete-from-cart-svg');

    })
})
