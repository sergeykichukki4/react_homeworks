import React from 'react';
import styles from './FavouriteStar.module.scss';

const FavouriteStarSVG = ({color, onClick, stroke, data, width="30", height="30" }) => {
    return (
        <button>
            <svg fill={color} stroke={stroke} onClick={onClick} data-testid={data} xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 24 24">
                <path
                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>
            </svg>
        </button>
    );
};

export default FavouriteStarSVG;