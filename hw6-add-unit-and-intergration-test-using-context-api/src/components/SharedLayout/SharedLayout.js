import React from 'react';
import Header from "../Header";
import {Outlet} from "react-router-dom";
import styles from './SharedLayout.module.scss';

const SharedLayout = ({cart,favourites}) => {
    return (<>
            <Header cart={cart} favourites={favourites}></Header>
            <section className={styles.section}>
                <Outlet/>
            </section>
        </>
    );
};

export default SharedLayout;