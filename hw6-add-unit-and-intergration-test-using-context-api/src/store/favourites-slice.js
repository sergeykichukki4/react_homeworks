import {createSlice} from "@reduxjs/toolkit";

const favouritesSlice = createSlice({
    name: 'favourites',
    initialState: {
        favouritesList: [],
        favouritesTotal: 0,
    },
    reducers: {
        addToFavourites (state, action) {
            state.favouritesList = [...state.favouritesList, action.payload];
            state.favouritesTotal += 1;
        },
        removeFromFavourites(state, action) {
            state.favouritesList = state.favouritesList.filter(item => item !== action.payload);
            state.favouritesTotal -= 1;
        }
    }
})

export const favouritesActions = favouritesSlice.actions;
export default favouritesSlice;