import {productsActions} from './products-slice';

export const fetchProducts = () => {
    return async (dispatch) => {
        const fetchHandler = async () => {
            const res = await fetch(`./game-devices.json`);
            return await res.json();
        }
        try {
            const {game_devices: gameDevices} = await fetchHandler();
            dispatch(productsActions.addProducts(gameDevices));
        } catch (err) {
            console.log(err);
        }
    }
}