import React from 'react';
import styles from './Favourites.module.scss';
import ProductItem from "../../components/ProductItem";
import {useSelector} from "react-redux";

const FavouritesPage = () => {
    const products = useSelector(state => state.products.productsList);
    const favourites = useSelector(state => state.favourites.favouritesList);

    return (
        <>
            <h3 className={styles.favouritesTitle}>Favourite Items</h3>
            <div className={styles.favouriteItemsWrapper}>
                {
                    favourites.length === 0 && <p className={styles.noFavProducts}>No favourite products</p>
                }
                {
                    favourites.length !== 0 && products.map((item) => {
                        if (favourites.includes(item.productArticle)) {
                            return <ProductItem key={item.productArticle}
                                                name={item.name}
                                                price={item.price}
                                                url={item.url}
                                                productArticle={item.productArticle}
                                                color={item.color}

                            />
                        }
                    })
                }
            </div>
        </>
    );
};

FavouritesPage.propTypes = {};

export default FavouritesPage;