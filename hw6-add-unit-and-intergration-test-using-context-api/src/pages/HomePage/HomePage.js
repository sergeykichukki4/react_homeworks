import React from 'react';
import styles from './HomePage.module.scss';
import {useSelector} from "react-redux";
import ProductItem from "../../components/ProductItem";
import Button from "../../components/Button";
import ProductItemTableView from "../../components/ProductItemTableView";
import {useViewUpdate, useView} from "../../components/ViewContext/ViewContext";

const HomePage = () => {
    const products = useSelector(state => state.products.productsList);
    const viewSwitcher = useView();
    const toggleViewSwitcher = useViewUpdate();
    console.log(viewSwitcher, toggleViewSwitcher)

    return (<>
            <h3 className={styles.title}>All products</h3>
            <div className={styles.allProductsWrapper}>
                {viewSwitcher &&
                    <Button onClick={toggleViewSwitcher} bgColor={'#7f90ff'} text={'Represent in Table view'}/>}
                {viewSwitcher && products.length !== 0 && products.map(item => {
                    return <ProductItem key={item.productArticle}
                                        name={item.name}
                                        price={item.price}
                                        url={item.url}
                                        productArticle={item.productArticle}
                                        color={item.color}/>
                })
                }
                {!viewSwitcher &&
                    <Button onClick={toggleViewSwitcher} bgColor={'#7f90ff'} text={'Represent in Cards view'}/>}
                {!viewSwitcher && <table>
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Color</th>
                        <th>Article</th>
                        <th>Favourite</th>
                    </tr>
                    </thead>
                    <tbody>
                    {products.map((item) => (
                        <ProductItemTableView key={item.productArticle}
                                              name={item.name}
                                              price={item.price}
                                              url={item.url}
                                              productArticle={item.productArticle}
                                              color={item.color}/>
                    ))}
                    </tbody>
                </table>}
            </div>
        </>
    );
};

export default HomePage;