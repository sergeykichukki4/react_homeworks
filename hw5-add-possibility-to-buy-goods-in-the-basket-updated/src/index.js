import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";
import {Provider} from "react-redux";
import {prepareInitState} from "./utils/init-state";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <ErrorBoundary>
        <React.StrictMode>
            <Provider store={prepareInitState()}>
                <App/>
            </Provider>
        </React.StrictMode>
    </ErrorBoundary>
);
