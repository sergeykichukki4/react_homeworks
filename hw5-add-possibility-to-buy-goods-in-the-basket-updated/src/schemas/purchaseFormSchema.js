import * as yup from 'yup';

const phoneRegExp  = /^\+380\d{9}$/;

export const purchaseForm = yup.object().shape({
    firstName: yup
        .string()
        .required('Required'),
    surname: yup
        .string()
        .required('Required'),
    age: yup
        .number()
        .positive()
        .integer()
        .min(16, 'You have to be at least 16 years old to make purchases')
        .required('Required'),
    location: yup
        .string()
        .oneOf(['kyiv', 'chernigiv', 'zhytomyr'], 'Choose the city from list')
        .required(),
    tel: yup
        .string()
        .matches(phoneRegExp , 'Invalid Ukrainian number')

})