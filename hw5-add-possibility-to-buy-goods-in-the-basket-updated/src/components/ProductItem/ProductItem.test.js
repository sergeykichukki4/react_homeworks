import {renderWithProviders} from "../../utils/test-utils";
import {fireEvent, screen} from "@testing-library/react";
import ProductItem from "./ProductItem";

test('should work', () => {
    renderWithProviders(<ProductItem
        key="key"
        name="name"
        price="12.98"
        url="http://localhost:3000/images/1.jpg"
        productArticle="productArticle"
        color="#ffaa00"
        quantity={1}
        totalPrice="64.9"
        inCart={true}
    />);
});

test('should open modal', () => {
    //init
    const result = renderWithProviders(<ProductItem
        key="key"
        name="name"
        price="12.98"
        url="http://localhost:3000/images/1.jpg"
        productArticle="productArticle"
        color="#ffaa00"
        quantity={1}
        totalPrice="64.9"
        inCart={true}
    />);

    //invoke
    fireEvent.click(screen.getByRole('button', { name: /Add to cart/i }));

    const modal = result.store.getState().modal;

    //check
    expect(modal).toEqual({
        isOpen: true,
        modalData: {
            actions: 'addToCart',
            header: 'Do you want to add this product to cart?',
            text: 'You can still delete the product from cart later',
        }
    });
});