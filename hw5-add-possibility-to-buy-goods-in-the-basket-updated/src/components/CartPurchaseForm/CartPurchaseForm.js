import styles from './CartPurchaseForm.module.scss';
import {purchaseForm} from '../../schemas/purchaseFormSchema';
import {Form, Formik} from "formik";
import CustomInput from "../CustomInput";
import CustomSelect from "../CustomSelect";
import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {cartActions} from "../../store/cart-slice";


const CartPurchaseForm = () => {
    const cartItems = useSelector(state => state.cart.cartList);
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);

    const onSubmit = async (values, actions) => {
        setLoading(true);
        setTimeout(async () => {
            await console.log(`Purchaser: \nname: ${values.firstName} \nsurname: ${values.surname} \nage: ${values.age} \nlocation: ${values.location.charAt(0).toUpperCase() + values.location.slice(1)} \ntelephone number: ${values.tel}`);
            await console.log('Products: ')
            await cartItems.forEach((item) => {
                console.log(`name: ${item.name} \nproductArticle: ${item.productArticle}\ncolor: ${item.color}\nquantity: ${item.quantity}\ntotalPrice: ${item.totalPrice}`)
            });
            await dispatch(cartActions.deleteAllItemsFromCart());
            await setLoading(false);
            actions.resetForm();
        }, 2000);

    };

    return (
        <>
            <h3>Purchase form: </h3>
            <Formik
                initialValues={{
                    firstName: '',
                    surname: '',
                    age: '',
                    location: '',
                    tel: '',
                }}
                onSubmit={onSubmit}
                validationSchema={purchaseForm}
            >
                {({isSubmitting}) => (
                    <Form>
                        <CustomInput
                            label='First name'
                            name='firstName'
                            type='text'
                            placeholder='Enter your name:'
                        />
                        <CustomInput
                            label='Surname'
                            name='surname'
                            type='text'
                            placeholder='Enter your surname:'
                        />
                        <CustomInput
                            label='Age'
                            name='age'
                            type='text'
                            placeholder='Enter your age:'
                        />
                        <CustomSelect
                            label='Location'
                            name='location'
                            placeholder='Choose your location:'
                        >
                            <option value="">Please select a city</option>
                            <option value="kyiv">Kyiv</option>
                            <option value="chernigiv">Chernigiv</option>
                            <option value="zhytomyr">Zhytomyr</option>
                            <option value="other">Other</option>
                        </CustomSelect>
                        <CustomInput
                            label='Mobile number'
                            name='tel'
                            type='tel'
                            placeholder='Enter your mobile number:'
                        />
                        <button className={styles.submitButton} disabled={isSubmitting} type="submit">
                            {loading ? (<div className={styles.loader}></div>) : 'Checkout'}
                        </button>

                    </Form>
                )}
            </Formik>

        </>
    );
};

export default CartPurchaseForm;