import {createSlice} from "@reduxjs/toolkit";

const productsSlice = createSlice({
    name: 'products',
    initialState: {
        productsList: [],
    },
    reducers: {
        addProducts (state, action) {
            state.productsList = action.payload;
        }
    }
})

export const productsActions = productsSlice.actions;
export default productsSlice;