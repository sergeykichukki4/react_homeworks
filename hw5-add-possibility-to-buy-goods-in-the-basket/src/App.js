import {useEffect} from 'react';
import './App.scss';
import Modal from './components/Modal/Modal';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import FavouritesPage from "./pages/FavouritesPage";
import CartPage from "./pages/CartPage";
import HomePage from "./pages/HomePage";
import ErrorPage from "./pages/ErrorPage/ErrorPage";
import SharedLayout from "./components/SharedLayout";
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from './store/products-action';

function App() {
    const isModalOpen = useSelector((state) => state.modal.isOpen);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchProducts());
    }, [])


    return (<>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<SharedLayout/>}>
                        <Route index element={<HomePage/>}/>
                        <Route path='cart' element={<CartPage/>}/>
                        <Route path='favourites' element={<FavouritesPage/>}/>
                        <Route path='*' element={<ErrorPage/>}/>
                    </Route>
                </Routes>
            </BrowserRouter>
            {isModalOpen && <Modal/>}
        </>
    );
}

export default App;
