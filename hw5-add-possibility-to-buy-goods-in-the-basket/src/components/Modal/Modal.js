import React from 'react';
import styles from "./Modal.module.scss";
import {useDispatch, useSelector} from "react-redux";
import {modalActions} from "../../store/modal-slice";


const Modal = () => {
    const modalData = useSelector((state) => state.modal.modalData);
    const {header, text, actions} = modalData;

    const dispatch = useDispatch();

    const closeModal = () => {
        dispatch(modalActions.closeModal());
    }
    const openModal = () => dispatch(modalActions.openModal({
        header,
        text,
        actions,
    }))

    return (
        <>
            <div className={styles.backgroundColor} onClick={(e) => {
                if (e.target.classList.contains(styles.backgroundColor)) {
                    closeModal();
                }
            }}>
                <div className={styles.modalContainer}>
                    <div className={styles.header}>
                        <div className={styles.title}>
                            <h2>{header}</h2>
                        </div>
                        <button onClick={closeModal} className={styles.cross}><img src="../cross_icon_white.svg"
                                                                                   width="30" height="30"
                                                                                   alt="cross-close"/></button>
                    </div>
                    <div className={styles.body}>
                        <p className={styles.text}>{text}</p>
                    </div>
                    <div className={styles.footer}>
                        {actions}
                    </div>
                </div>
            </div>
        </>
    )
}

export default Modal;
