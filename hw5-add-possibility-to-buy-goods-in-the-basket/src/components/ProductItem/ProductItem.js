import React from 'react';
import styles from './ProductItem.module.scss';
import Button from "../Button";
import {useState} from "react";
import FavouriteStarSVG from "../FavouriteStarSVG";
import DeleteBinSVG from "../DeleteBinSVG";
import {modalActions} from "../../store/modal-slice";
import {useDispatch, useSelector} from "react-redux";
import {cartActions} from "../../store/cart-slice";
import {favouritesActions} from "../../store/favourites-slice";

const ProductItem = (props) => {
    const favourites = useSelector(state => state.favourites.favouritesList);
    const dispatch = useDispatch();
    const {name, price, url, color, productArticle, inCart, quantity} = props;
    const setUpColor = () => {
        if (favourites.includes(productArticle)) {
            return '#ffaa00';
        }
        return 'white';
    }

    const [starFavSVG, setStarFavSVG] = useState(() => setUpColor());

    const colorToggleHandler = () => setStarFavSVG(prevColor => (prevColor === '#ffaa00' ? 'white' : '#ffaa00'));

    const toggleFavourites = () => {
        const favouritesCurrentArr = [...favourites];
        colorToggleHandler();
        if (favouritesCurrentArr.includes(productArticle)) {
            favouritesCurrentArr.splice(favouritesCurrentArr.indexOf(productArticle), 1);
            dispatch(favouritesActions.removeFromFavourites(productArticle));

        } else {
            favouritesCurrentArr.push(productArticle);
            dispatch(favouritesActions.addToFavourites(productArticle));
        }
        localStorage.setItem('favouritesIDs', JSON.stringify(favouritesCurrentArr));
    }
    const addToCart = () => {
        dispatch(cartActions.addToCart({
            name,
            price,
            url,
            productArticle,
            color,
        }))
        closeModal();
    }
    const deleteFromCart = () => {
        dispatch(cartActions.deleteFromCart(productArticle));
        closeModal();
    }
    const openModal = ({header, text, actions}) => {
        dispatch(modalActions.openModal({
            header,
            text,
            actions,
        }))
    }

    const closeModal = () => dispatch(modalActions.closeModal());

    return (
        <div className={styles.productItem}>
            <img src={url} alt={name} className={styles.productImage}/>
            <div className={styles.productInfo}>
                <h3 className={styles.productName}>{name}</h3>
                <p className={styles.productPrice}>{price}</p>
                <p className={styles.productColor}>Color: {color}</p>
                <Button onClick={() => openModal({
                    header: 'Do you want to add this product to cart?',
                    text: (
                        <>
                            You can still delete the product from cart later.
                            <br/>
                            Are you sure you want to add it to Cart?
                        </>
                    ),
                    actions: (<>
                        <Button bgColor={'#b43725'} text={'Yes'} onClick={() => addToCart({
                            name,
                            price,
                            url,
                            productArticle,
                            color,
                        })}/>
                        <Button bgColor={'#b43725'} text={'Cancel'} onClick={() => closeModal()}/>
                    </>)

                })} bgColor={'red'} text={'Add to cart'}/>
                {!inCart &&
                    <FavouriteStarSVG className={styles.productFavourite} color={starFavSVG} onClick={toggleFavourites}
                                      stroke={'black'}/>
                }
                {
                    inCart && <DeleteBinSVG className={styles.delFromCart} color={'#000000'} onClick={() => openModal({
                        header: 'Do you want to add this product to cart?',
                        text: (
                            <>
                                You can always add the product on home page later.
                                <br/>
                                Are you sure you want to delete the product from Cart?
                            </>
                        ),
                        actions: (<>
                            <Button bgColor={'#b43725'} text={'Yes'} onClick={() => deleteFromCart(productArticle)}/>
                            <Button bgColor={'#b43725'} text={'Cancel'} onClick={() => closeModal()}/>
                        </>)

                    })}/>
                }
                {
                    inCart && <p>item quantity: {quantity}</p>
                }
            </div>
        </div>
    );
};

export default ProductItem;