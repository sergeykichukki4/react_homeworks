import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import styles from './Navbar.module.scss';
const Navbar = props => {
    return (
        <nav className={styles.navbar}>
            <Link className={styles.navItem} to='/'>Home</Link>
            <Link className={styles.navItem} to='cart'>Cart</Link>
            <Link className={styles.navItem} to='favourites'>Favourites</Link>
        </nav>
    );
};

Navbar.propTypes = {};

export default Navbar;