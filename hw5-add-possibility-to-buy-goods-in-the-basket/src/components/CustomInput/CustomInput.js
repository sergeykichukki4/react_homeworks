import { useField } from "formik";
import styles from './CustomInput.module.scss';
import React from 'react';


const CustomInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);

    return (
        <>
            <label>{label}</label>
            <input
                {...field}
                {...props}
                className={meta.touched && meta.error ? `${styles.inputError}` : ""}
            />
            {meta.touched && meta.error && <div className={styles.error}>{meta.error}</div>}
        </>
    );
};
export default CustomInput;