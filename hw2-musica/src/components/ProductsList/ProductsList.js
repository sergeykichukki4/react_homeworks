import React from 'react';
import PropTypes from 'prop-types';
import ProductItem from "../ProductItem";
import styles from './ProductsList.module.scss'

const ProductsList = (props) => {
    const {products, addToCart, changeFavourites, openModal, closeModal} = props;

    return (
        <div className={styles.productListWrapper}>
            {products.map((product) => {
                return <ProductItem key={product.productArticle}
                                    product={product}
                                    addToCart={addToCart}
                                    changeFavourites={changeFavourites}
                                    openModal={openModal}
                                    closeModal={closeModal}
                />
            })}
        </div>
    );
};

ProductsList.propTypes = {};

export default ProductsList;