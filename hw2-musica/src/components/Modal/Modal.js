import React from 'react';
import PropTypes from "prop-types";
import styles from "./Modal.module.scss";


const Modal = (props) => {
    const {header, closeButton, text, actions, closeModal} = props;

    return (
        <>
            <div className={styles.backgroundColor} onClick={(e) => {
                if(e.target.classList.contains(styles.backgroundColor)) {
                    closeModal();
                }
            }} >
                <div className={styles.modalContainer}>
                    <div className={styles.header}>
                        <div className={styles.title}>
                            <h2>{header}</h2>
                        </div>
                        {closeButton && <button onClick={closeModal} className={styles.cross}><img src="../cross_icon_white.svg" width="30" height="30" alt="cross-close"/></button>}
                    </div>
                    <div className={styles.body}>
                        <p className={styles.text}>{text}</p>
                    </div>
                    <div className={styles.footer}>
                        {actions}
                    </div>
                </div>
            </div>
        </>
    )
}

Modal.propTypes = {
    closeModal: PropTypes.func,
    closeButton: PropTypes.bool,
    header: PropTypes.string.isRequired,
    text: PropTypes.node,
    actions: PropTypes.node.isRequired,
}

Modal.defaultProps = {
    closeButton: false,
    text: '',
}

export default Modal;
