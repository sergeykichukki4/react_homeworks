import React from 'react';
import PropTypes from 'prop-types';
import styles from './ProductItem.module.scss';
import Button from "../Button";
import {favouritesArr} from '../../App';
import {useState} from "react";
import FavouriteStar from "../FavouriteStar";

const ProductItem = (props) => {
    const {product, addToCart, changeFavourites, openModal, closeModal} = props;
    const {name, price, url: imgUrl, color} = product;

    const setUpColor = () => {
        if (localStorage.getItem('favouritesIDs')) {
            const setUpColorArr = JSON.parse(localStorage.getItem('favouritesIDs'));
            if (setUpColorArr.includes(product.productArticle)) {
                return 'yellow';
            }
        }
        return 'white';
    }

    const [starFavSVG, setStarFavSVG] = useState(() => setUpColor());
    const colorToggleHandler = () => {
        setStarFavSVG(prevColor => (prevColor === 'yellow' ? 'white' : 'yellow'));
    }
    //add SVG color change
    const toggleFavourites = () => {
        colorToggleHandler();
        if (favouritesArr.includes(product.productArticle)) {
            favouritesArr.splice(favouritesArr.indexOf(product.productArticle), 1);

        } else {
            favouritesArr.push(product.productArticle)
        }
        localStorage.setItem('favouritesIDs', JSON.stringify(favouritesArr));

        changeFavourites();
    }

    return (
            <div className={styles.productItem}>
                <img src={imgUrl} alt={name} className={styles.productImage}/>
                <div className={styles.productInfo}>
                    <h3 className={styles.productName}>{name}</h3>
                    <p className={styles.productPrice}>{price}</p>
                    <p className={styles.productColor}>Color: {color}</p>
                    <Button onClick={() => openModal({
                        header: 'Do you want to add this product to cart?',
                        closeButton: true,
                        text: (
                            <>
                                You can still delete the product from cart later.
                                <br />
                                Are you sure you want to add it to Cart?
                            </>
                        ),
                        actions: (<>
                            <Button bgColor={'#b43725'} text={'Yes'} onClick={() => addToCart()}/>
                            <Button bgColor={'#b43725'} text={'Cancel'} onClick={() => closeModal()}/>
                        </>)

                    })} bgColor={'red'} text={'Add to cart'}/>
                    <FavouriteStar className={styles.productFavourite} color={starFavSVG} onClick={toggleFavourites} stroke={'black'}/>

                </div>
            </div>
    );
};

ProductItem.propTypes = {
    // name: PropTypes.string.isRequired,
    // price: PropTypes.number.isRequired,
    // url: PropTypes.string.isRequired,
    // color: PropTypes.string.isRequired,
    // isFavourite: PropTypes.bool.isRequired,
    addToCart: PropTypes.func,
};

export default ProductItem;