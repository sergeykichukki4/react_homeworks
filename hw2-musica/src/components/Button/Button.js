import React from 'react';
import PropTypes from "prop-types";
import styles from './Buttons.module.scss'

const Button = (props) => {
    const {bgColor, text, onClick, type} = props;

    return (
        <>
            <button  className={styles.btn} style={{
                backgroundColor: bgColor,
                color: '#FFFFFF',
            }} onClick={onClick} type={type}>{text}</button>
        </>
    )
}

Button.propTypes = {
    onClick: PropTypes.func.isRequired,
    bgColor: PropTypes.string,
    test: PropTypes.string,
}

Button.defaultProps = {
    type: 'button',
    bgColor: 'white',
    test: '',
}
export default Button;