import {useState, useEffect} from 'react';
import './App.scss';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';
import ProductsList from "./components/ProductsList";
import Header from "./components/Header";

export const favouritesArr = JSON.parse(localStorage.getItem('favouritesIDs')) ?? [];
export const productsCartArr = JSON.parse(localStorage.getItem('productsCartArr')) ?? [];

function App() {
    const [isModalOpen, setOpenModal] = useState(false);
    const [modalData, setModalData] = useState({
        header: '',
        closeButton: false,
        text: '',
        actions: null,
    });
    const {header, closeButton, text, actions} = modalData;

    const [products, setProducts] = useState([]);

    //have to add logic on addToCart Modal
    const [cart, setCart] = useState(productsCartArr);
    const [favourites, setFavourites] = useState(false);

    useEffect(() => {
        const fetchProducts = async () => {
            const {game_devices: gameDevices} = await fetch(`./game-devices.json`).then(res => res.json());
            setProducts(gameDevices);
            if (products.length !== 0) {
                products.forEach(item => {
                    const key = item.productArticle;
                    const value = JSON.stringify(item);
                    localStorage.setItem(key, value);
                })
            }
        }

        fetchProducts()
            .catch(console.error);
    }, [products.length]);


    const openModal = ({header, closeButton, text, actions}) => {
        setOpenModal(true);
        setModalData({
            header,
            closeButton,
            text,
            actions,
        })
    }

    const closeModal = () => setOpenModal(false);

    const addToCart = (product) => {
        setCart([...cart, product]);
        localStorage.setItem('productsCartArr', JSON.stringify([...cart, product]));
        closeModal();
    }

    const changeFavourites = () => setFavourites(prev => !prev);

    return (<>
            <div className="App">
                <Header cart={cart}/>
                {
                    products.length !== 0 && <ProductsList products={products} addToCart={addToCart} changeFavourites={changeFavourites} openModal={openModal} closeModal={closeModal}/>
                }
                {
                    isModalOpen && <Modal header={header} closeButton={closeButton} text={text} actions={actions} closeModal={closeModal}/>
                }
            </div>
        </>
    );
}

export default App;
