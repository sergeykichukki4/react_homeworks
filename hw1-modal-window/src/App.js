import React from 'react';
import {useState} from 'react';
import './App.scss';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';

function App() {
    const [isModalOpen, setOpenModal] = useState(false);
    const [modalData, setModalData] = useState({
        header: '',
        closeButton: false,
        text: '',
        actions: null,
    });

    const { header, closeButton, text, actions } = modalData;

    const openModal = ({header, closeButton, text, actions}) => {
        setOpenModal(true);
        setModalData({
            header,
            closeButton,
            text,
            actions,
        })
    }
    const closeModal = () => setOpenModal(false);

    return (
        <div className="App">
            <Button bgColor={'red'}
                    text={'Delete'}
                    onClick={() => openModal({
                        header: 'Do you want to delete this file?',
                        closeButton: true,
                        text: (
                            <>
                                Once you delete this file, it won’t be possible to undo this action.
                                <br />
                                Are you sure you want to delete it?
                            </>
                        ),
                        actions: (<>
                            <Button bgColor={'#b43725'} text={'Ok'} onClick={() => closeModal()}/>
                            <Button bgColor={'#b43725'} text={'Delete'} onClick={() => closeModal()}/>
                        </>)
                    })}>
            </Button>

            <Button bgColor={'green'}
                    text={'Test'}
                    onClick={() => openModal({
                        header: 'Another modal',
                        closeButton: false,
                        text: 'Test',
                        actions: (<>
                            <Button bgColor={'#005e80'} text={'Press it'} onClick={() => {
                                closeModal();
                            }}/>
                        </>)
                    })}>
            </Button>

            {
                isModalOpen && <Modal header={header} closeButton={closeButton} text={text} actions={actions} closeModal={closeModal}/>
            }
        </div>
    );
}

export default App;
