import {configureStore} from "@reduxjs/toolkit";
import modalSlice from "./modal-slice";
import productsSlice from "./products-slice";
import cartSlice from "./cart-slice";
import favouritesSlice from "./favourites-slice";

const store = configureStore({
    reducer: {
        modal: modalSlice.reducer,
        products: productsSlice.reducer,
        cart: cartSlice.reducer,
        favourites: favouritesSlice.reducer,
    }
})

export default store;