import {createSlice} from "@reduxjs/toolkit";

const modalSlice = createSlice({
    name: 'modal',
    initialState: {
        isOpen: false,
        modalData: {
            header: '',
            text: '',
            actions: null,
        }
    },
    reducers: {
        openModal: (state, action) => {
            state.isOpen = true;
            state.modalData = action.payload;
        },
        closeModal: (state) => {
            state.isOpen = false;
        }
    }
})

export const modalActions = modalSlice.actions;
export default modalSlice;