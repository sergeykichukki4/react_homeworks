import {createSlice} from "@reduxjs/toolkit";

const cartSlice = createSlice({
    name: 'cart',
    initialState: {
        cartList: JSON.parse(localStorage.getItem('productsCartArr')) ?? [],
        totalQuantity: JSON.parse(localStorage.getItem('productsCartArr')).length ?? 0,
    },
    reducers: {
        addToCart(state, action) {
            const newItem = action.payload;
            let existingItem = false;

            state.cartList.forEach(item => {
                if (item.productArticle === newItem.productArticle) {
                    existingItem = true;
                    item.quantity++;
                    item.totalPrice += +newItem.price;
                }
            });

            if (!existingItem) {
                state.cartList.push({
                    name: newItem.name,
                    price: +newItem.price,
                    url: newItem.url,
                    productArticle: newItem.productArticle,
                    color: newItem.color,
                    quantity: 1,
                    totalPrice: +newItem.price,
                });
            }

            state.totalQuantity++;
            localStorage.setItem('productsCartArr', JSON.stringify(state.cartList));
        },
        deleteFromCart(state, action) {
            const id = action.payload;
            const updatedCartList = [];

            state.cartList.forEach(item => {
                if (item.productArticle !== id) {
                    updatedCartList.push(item);
                } else if (item.quantity > 1) {
                    updatedCartList.push({
                        ...item,
                        quantity: item.quantity -= 1,
                        totalPrice: item.totalPrice -= +item.price,
                    });
                }
            });

            state.cartList = updatedCartList;
            state.totalQuantity--;
            localStorage.setItem('productsCartArr', JSON.stringify(updatedCartList));
        }
    }
})

export const cartActions = cartSlice.actions;
export default cartSlice;