import React from 'react';
import PropTypes from 'prop-types';
import Header from "../Header";
import {Outlet} from "react-router-dom";
import styles from './SharedLayout.module.scss';

const SharedLayout = ({cart,favourites}) => {
    return (<>
            <Header cart={cart} favourites={favourites}></Header>
            <section className={styles.section}>
                <Outlet/>
            </section>
        </>
    );
};

SharedLayout.propTypes = {};

export default SharedLayout;