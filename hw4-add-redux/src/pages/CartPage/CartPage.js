import React from 'react';
import styles from './CartPage.module.scss';
import ProductItem from "../../components/ProductItem";
import {useSelector} from "react-redux";

const CartPage = () => {
    const cartItems = useSelector(state => state.cart.cartList);
    const inCart = true;
        return (
            <>
                <h3 className={styles.title}>Cart items</h3>
                <div className={styles.cartItemWrapper}>
                    {
                        cartItems.length === 0 && <p className={styles.noCartItems}>Cart is empty</p>
                    }
                    {
                        cartItems.length !== 0 && cartItems.map(item => {
                            return <ProductItem key={item.productArticle}
                                                name={item.name}
                                                price={item.price}
                                                url={item.url}
                                                productArticle={item.productArticle}
                                                color={item.color}
                                                quantity={item.quantity}
                                                totalPrice={item.totalPrice}
                                                inCart={inCart}
                            />
                        })
                    }
                </div>
            </>
        );
};

export default CartPage;