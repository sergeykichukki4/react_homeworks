import React from 'react';
import styles from './HomePage.module.scss';
import {useSelector} from "react-redux";
import ProductItem from "../../components/ProductItem";

const HomePage = () => {
    const products = useSelector(state => state.products.productsList);

    return (<>
            <h3 className={styles.title}>All products</h3>
            <div className={styles.allProductsWrapper}>
                {
                    products.length !== 0 && products.map(item => {
                        return <ProductItem key={item.productArticle}
                                            name={item.name}
                                            price={item.price}
                                            url={item.url}
                                            productArticle={item.productArticle}
                                            color={item.color}/>
                    })
                }
            </div>
        </>
    );
};

export default HomePage;